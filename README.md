# substrings

Assignment #2 Ruby Lesson [The odin project]
Implement a method #substrings that takes a word as the first argument and then an array of valid substrings (your dictionary) as the second argument. It should return a hash listing each substring (case i